import {ModuleWithProviders, NgModule, Optional, SkipSelf} from '@angular/core';
import {MostModule} from '@themost/angular';
import {AdvancedFormResolver, AdvancedFormsService} from './advanced-forms.service';
import {HttpClientModule} from '@angular/common/http';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../environments/environment';
import {FORMS_LOCALES} from './i18n';
import {FormioAppConfig, FormioModule} from 'angular-formio';
import { AdvancedFormComponent } from './advanced-form.component';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {AdvancedFormItemResolver, AdvancedFormModalComponent, AdvancedFormModelResolver, AdvancedFormParentItemResolver, AdvancedFormRouteItemResolver} from './advanced-form-modal.component';
import {AppConfig} from '../formio-config';
import { ErrorModule, SharedModule } from '@universis/common';
import { AdvancedFormContainerComponent } from './advanced-form-container/advanced-form-container.component';
import { AdvancedFormItemWithLocalesResolver, AdvancedFormRouterComponent } from './advanced-form-router/advanced-form-router.component';



@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    MostModule,
    TranslateModule,
    FormioModule,
    RouterModule,
    SharedModule,
    ErrorModule
  ],
  declarations: [
    AdvancedFormComponent,
    AdvancedFormModalComponent,
    AdvancedFormContainerComponent,
    AdvancedFormRouterComponent
  ],
  exports: [
      AdvancedFormComponent,
      AdvancedFormModalComponent,
      AdvancedFormRouterComponent
  ]
})
export class AdvancedFormsModule {

  constructor( @Optional() @SkipSelf() parentModule: AdvancedFormsModule, private _translateService: TranslateService) {
    environment.languages.forEach( language => {
      if (FORMS_LOCALES.hasOwnProperty(language)) {
        this._translateService.setTranslation(language, FORMS_LOCALES[language], true);
      }
    });
  }

  static forRoot(): ModuleWithProviders<AdvancedFormsModule> {
    return {
      ngModule: AdvancedFormsModule,
      providers: [
        AdvancedFormsService,
        AdvancedFormResolver,
        AdvancedFormParentItemResolver,
        AdvancedFormRouteItemResolver,
        AdvancedFormItemWithLocalesResolver,
        AdvancedFormItemResolver,
        AdvancedFormModelResolver,
        {provide: FormioAppConfig, useValue: AppConfig}
      ]
    };
  }
}
