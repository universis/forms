import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Injectable,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { ButtonTypes, RouterModalOkCancel } from '@universis/common/routing';
import { FormioComponent, FormioRefreshValue } from 'angular-formio';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { ErrorService, TemplatePipe, AppEventService } from '@universis/common';
import {
  AdvancedFormsService,
  ServiceUrlPreProcessor,
  QueryParamsPreProcessor,
  ActivatedRouteDataPreProcessor,
  EmptyValuePostProcessor,
  ClearOnHidePostProcessor,
  AddDatetimeLocalePreProcessor
} from './advanced-forms.service';
import { Args, DataServiceQueryParams } from '@themost/client';
import { AngularDataContext } from '@themost/angular';

export interface AdvancedFormModalOptions {
  modalClass?: string;
  modalTitle?: string;
}

export interface AdvancedFormModalData {
  model?: string;
  action?: string;
  data?: any;
  formConfig?: any;
  modalOptions?: AdvancedFormModalOptions;
  serviceQueryParams?:  {
    $filter?: string;
    $groupby?: string;
    $select?: string;
    $orderby?: string;
    $expand?: string;
    $count?: boolean;
    $top?: number;
    $skip?: number;
    $first?: boolean;
    $levels?: number;
};
  closeOnSubmit?: boolean;
  continueLink?: string;
}

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'advanced-form-edit-modal',
  template: `<formio [form]="formConfig" [language]="setLanguage" (change)="onChange($event)" [submission]="{ data: formData }"
                     (formLoad)="onLoad($event)" [refresh]="refreshForm" [renderOptions]="renderOptions" #form></formio>`,
  styles: [
    `
      .form-control:disabled, .form-control[readonly] {
            background-color: inherit;
        }
        .form-check.checkbox {
          position: relative
        }
    `
  ],
  encapsulation: ViewEncapsulation.None
})
export class AdvancedFormModalComponent extends RouterModalOkCancel implements OnInit, OnDestroy, AfterViewInit {

  // formData holds form.io definition that is going to be loaded from json
  private dataSubscription: Subscription;
  public formConfig: any;
  public formData: any = {};
  public renderOptions: {
    language: string;
    i18n: any;
  };
  private translation: any = {};
  @ViewChild('form') form: FormioComponent;
  @Output() refreshForm: EventEmitter<FormioRefreshValue> = new EventEmitter();
  @Output() setLanguage: EventEmitter<string> = new EventEmitter();
  @Input('model') model: string;
  @Input('action') action: string;
  @Input('closeOnSubmit') closeOnSubmit: boolean;
  @Input('continueLink') continueLink: string;

  constructor(protected router: Router,
              protected activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _formService: AdvancedFormsService,
              private _translateService: TranslateService,
              private _errorService: ErrorService,
              private _element: ElementRef,
              private _appEvent: AppEventService) {
    super(router, activatedRoute);
    this.modalClass = 'modal-xl';
  }

  onLoad(event) {
    // try to set translated modal title
    if (this.formConfig.title) {
      this.modalTitle = this.translation[this.formConfig.title] || this.formConfig.title;
    }
    this.okButtonText = this._translateService.instant('Forms.Submit');
    this.cancelButtonText = this._translateService.instant('Forms.Cancel');
    // restore buttons
    setTimeout(() => {
      this.okButtonClass = ButtonTypes.ok.buttonClass;
      this.cancelButtonClass = ButtonTypes.cancel.buttonClass;
    }, 100);
  }

  ngOnInit(): void {
    //
  }

  ngAfterViewInit(): void {
    // hide buttons
    this.okButtonClass = 'd-none';
    this.cancelButtonClass = 'd-none';
    // get metadata
    this._context.getMetadata().then((schema) => {
      // subscribe for route data
      this.dataSubscription = this.activatedRoute.data.subscribe((routeData: AdvancedFormModalData) => {
        // validate params
        Args.check(routeData.model != null, 'Expected a valid data model.');
        Args.check(routeData.action != null, 'Expected a valid data action.');
        this.model = routeData.model;
        this.action = routeData.action;
        this.closeOnSubmit = routeData.closeOnSubmit;
        this.continueLink = routeData.continueLink;
        // load form
        (() => {
          // if route contains form configuration return it
          if (routeData.formConfig) {
            // apply pre-processors
            new AddDatetimeLocalePreProcessor(this._translateService).parse(routeData.formConfig);
            new ServiceUrlPreProcessor(this._context).parse(routeData.formConfig);
            new QueryParamsPreProcessor(this.activatedRoute, this._context).parse(routeData.formConfig);
            return Promise.resolve(routeData.formConfig);
          }
          // otherwise do the default action to load form by a url
          return this._formService.loadForm(`${routeData.model}/${routeData.action}`);
        })().then(formConfig => {
          // get styling attributes
          if (routeData && routeData.modalOptions) {
            Object.assign(this, routeData.modalOptions);
          }
          // find submit button
          const findButton = formConfig.components.find(component => {
            return component.type === 'button' && component.key === 'submit';
          });
          // hide button
          if (findButton) {
            (<any>findButton).hidden = true;
          }
          if (routeData.data) {
            // set only ActivatedRoute.data if exists
            this.formData = routeData.data;
          } else {
            // call activated route pre-processor to parse data
            return new ActivatedRouteDataPreProcessor(this.activatedRoute, this._context)
              .parseAsync({
                model: formConfig.model
              }).then(form => {
                // set data
                this.formData = form.data != null ? form.data : {};
                // set form data
                this.formConfig = formConfig;
                // initialize render options
                this.initializeRenderOptions();
                // do refresh
                this.refreshForm.emit({
                  submission: {
                    data: this.formData
                  },
                  form: this.formConfig
                });
              });
          }
          // set form data
          this.formConfig = formConfig;
          // initialize render options
          this.initializeRenderOptions();
          // do refresh
          this.refreshForm.emit({
            submission: {
              data: this.formData
            },
            form: this.formConfig
          });
        }, err => {
          return this._errorService.showError(err);
        });
      });
    });
  }

  ngOnDestroy() {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

  onChange(event: any) {
    // handle changes (check if event has isValid property)
    if (Object.prototype.hasOwnProperty.call(event, 'isValid')) {
      // enable or disable button based on form status
      this.okButtonDisabled = !event.isValid;
    }
  }

  initializeRenderOptions() {
    const currentLang = this._translateService.currentLang;
    const translation = this._translateService.instant('Forms');
    if (this.formConfig && this.formConfig.settings && this.formConfig.settings.i18n) {
      // try to get local translations
      if (Object.prototype.hasOwnProperty.call(this.formConfig.settings.i18n, currentLang)) {
        // assign translations
        Object.assign(translation, this.formConfig.settings.i18n[currentLang]);
      }
    }
    // prepare i18n object
    const i18n = Object.defineProperty({}, currentLang, {
      configurable: true,
      enumerable: true,
      writable: true,
      value: translation
    });
    this.translation = translation;
    // and initialize renderOptions
    this.renderOptions = {
      language: currentLang,
      i18n
    };
  }

  async ok() {
    // post item
    return await (async (submissionData: any) => {
      try {
        new ClearOnHidePostProcessor().parse(this.formConfig.components, submissionData.data);
        new EmptyValuePostProcessor().parse(this.formConfig, submissionData.data);
        return await this._context.getService().execute({
          method: 'POST',
          url: this.model,
          headers: {},
          data: submissionData.data
        });
      } catch (error) {
        throw error;
      }
    })(this.form.submission).then((res) => {
      this.form.onSubmit(this.form.submission, true);
      this._appEvent.change.next({
        model: this.formConfig.model,
        target: res
      });
      try {
        if (this.continueLink) {
          return this.router.navigate([new TemplatePipe().transform(this.continueLink, res)]);
        }
      } catch (error) { }
      // validate closeOnSubmit
      if (this.closeOnSubmit) {
        return this.close({
          fragment: 'reload',
          skipLocationChange: true
        });
      }
    }).catch(err => {
      if (err.status) {
        const translatedError = this._translateService.instant(`E${err.status}`);
        if (err.status >= 400 && err.status < 500) {
          if (err.error) {
            translatedError.message = err.error.innerMessage || err.error.message;
          }
        }
        return this.onError(new Error(translatedError.message));
      }
      this.onError(err);
    });
  }

  onError(error) {
    this.form.onError(error);
  }

  cancel() {
    // cancel and close form
    return this.close();
  }

}
@Injectable()
export class AdvancedFormItemResolver implements Resolve<any> {
  constructor(private _context: AngularDataContext) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<any> | any {
    const model = route.data.model || route.params.model;
    if (model) {
      // get primary key
      return this._context.getMetadata().then(schema => {
        const testName = new RegExp(`^${model}$`, 'i');
        // find entity set
        const findEntitySet = schema.EntityContainer.EntitySet.find(x => {
          return testName.test(x.Name);
        });
        if (findEntitySet) {
          const findEntityType = schema.EntityType.find(x => {
            return x.Name === findEntitySet.EntityType;
          });
          if (findEntityType) {
            // get primary key
            const key = (findEntityType.Key && findEntityType.Key.PropertyRef[0] && findEntityType.Key.PropertyRef[0].Name) || 'id';
            Args.check(key != null, 'Expected a valid primary key');
            const query = this._context.model(findEntitySet.Name).asQueryable(route.data.serviceQueryParams || {}).prepare();
            return query.where(key).equal(route.params.id).getItem();
          }
        }
      });
    }
    // todo::throw error for missing or invalid model
    return null;
  }
}

@Injectable()
export class AdvancedFormParentItemResolver implements Resolve<any> {
  constructor(private _context: AngularDataContext) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<any> | any {
    let model = null;
    // get model from parent data or params
    if (route.parent) {
      model = route.parent.data.model || route.parent.params.model;
    }
    if (model) {
      // get primary key
      return this._context.getMetadata().then(schema => {
        const testName = new RegExp(`^${model}$`, 'i');
        // find entity set
        const findEntitySet = schema.EntityContainer.EntitySet.find(x => {
          return testName.test(x.Name);
        });
        if (findEntitySet) {
          const findEntityType = schema.EntityType.find(x => {
            return x.Name === findEntitySet.EntityType;
          });
          if (findEntityType) {
            // get primary key
            const key = (findEntityType.Key && findEntityType.Key.PropertyRef[0] && findEntityType.Key.PropertyRef[0].Name) || 'id';
            Args.check(key != null, 'Expected a valid primary key');
            const query = this._context.model(findEntitySet.Name).asQueryable(route.data.serviceQueryParams || {}).prepare();
            return query.where(key).equal(route.params.id).getItem();
          }
        }
      });
    }
    // todo::throw error for missing or invalid model
    return null;
  }
}

@Injectable()
export class AdvancedFormRouteItemResolver implements Resolve<any> {
  constructor(private _context: AngularDataContext) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<any> | any {
    let model = null;
    let routeParent = route.parent;
    // get model from parent data or params
    do {
      // code block to be executed
      routeParent = routeParent.parent;
      model = routeParent.data.model || routeParent.params.model;
    } while (routeParent.parent === null);
    if (model) {
      // get primary key
      return this._context.getMetadata().then(schema => {
        const testName = new RegExp(`^${model}$`, 'i');
        // find entity set
        const findEntitySet = schema.EntityContainer.EntitySet.find(x => {
          return testName.test(x.Name);
        });
        if (findEntitySet) {
          const findEntityType = schema.EntityType.find(x => {
            return x.Name === findEntitySet.EntityType;
          });
          if (findEntityType) {
            // get primary key
            const key = (findEntityType.Key && findEntityType.Key.PropertyRef[0] && findEntityType.Key.PropertyRef[0].Name) || 'id';
            Args.check(key != null, 'Expected a valid primary key');
            const query = this._context.model(findEntitySet.Name).asQueryable(routeParent.data.serviceQueryParams || {}).prepare();
            return query.where(key).equal(routeParent.params.id).getItem();
          }
        }
      });
    }
    // todo::throw error for missing or invalid model
    return null;
  }
}

@Injectable()
export class AdvancedFormModelResolver implements Resolve<string> {
  constructor(private _context: AngularDataContext) { }
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<any> | any {
    const model = route.params.model;
    if (model) {
      // get primary key
      return this._context.getMetadata().then(schema => {
        const testName = new RegExp(`^${model}$`, 'i');
        // find entity set
        const findEntitySet = schema.EntityContainer.EntitySet.find(x => {
          return testName.test(x.Name);
        });
        if (findEntitySet) {
          return Promise.resolve(findEntitySet.Name);
        }
        return Promise.resolve(null);
      });
    }
    // todo::throw error for missing or invalid model
    return Promise.resolve(null);
  }
}
