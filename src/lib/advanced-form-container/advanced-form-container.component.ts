import {Component, Input, OnInit, ViewChild} from '@angular/core';
import { AdvancedFormComponent } from '../advanced-form.component';
import { fadeAnimation } from '../animations';

@Component({
  selector: 'advanced-form-container',
  templateUrl: './advanced-form-container.component.html',
  animations: [
    fadeAnimation
  ]
})
export class AdvancedFormContainerComponent implements OnInit {

  @Input() data: any;
  @Input() src: any;
  @ViewChild('form') form: AdvancedFormComponent;

  constructor() { }

  ngOnInit() {
    //
  }

  async onCompletedSubmission(event: any): Promise<void> {
    //
  }

}
